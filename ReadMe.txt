Projekt:    poker, TI1-P1 SS14
Organisation:   HAW-Hamburg, Informatik
Team:       S1T5
Autor(en):
Baha, Mir Farshid   (mirfarshid.baha@haw-hamburg.de)
Cakir, Mehmet     	(mehmet.cakir@haw-hamburg.de)

Review History
===========================
140422 v1.0 Schafers ok
Anregung: 
- bewusste Testcases zusammenstellen
- Hilfsdatenstruktur f�r leichtere Auswertung
- einzeilige Ausgabe in einer Zeile, wo die Karten aufgef�hrt werden
- in den Methoden Ausgaben und Berechnungen trennen
- schon mal im Auge haben Test und Berechnung trennen


140603 v2.0 ok schafers
Bemerkung fuers Leben
Viel zu wenige geeignete Hilfsdatenstrukturen => Konsequenz "unnoetig" komplizierter Code
"public Card[] lookForStraight(){..." voellig unwartbar
"konzept" keine klare Code -Struktur (1 gemixte Iteration, die eigentlich 2 Iterationen realisiert; Im "Fertigfall" weiter machen (nur Aenderungen unterdruecken) fuehrt zu sehr schwierigen Code)
Ziel sollte immer max. einfacher Code sein
