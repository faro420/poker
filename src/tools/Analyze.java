package tools;

/**
 * The Analyze class works primary with the auxiliary data structure in form of an 4x13 array.
 * 
 * Introduction
 * This class contains following methods, to check different combinations:
 * lookForSFlush         = checks if there is a STRAIGHT_FLUSH
 * lookForQuads          = checks if there is a QUAD   
 * lookForFullHouse      = checks if there is a FULL_HOUSE           
 * lookForFlush          = checks if there is a FLUSH                
 * lookForStraight       = checks if there is a STRAIGHT             
 * lookForStraightWheel  = checks if there is a STRAIGHT_WHEEL
 * lookForTrips          = checks if there is a TRIP 
 * lookForTwoPairs       = checks if there are TWO_PAIRS 
 * lookForOnePair        = checks if there is ONE_PAIR        
 * lookForHighCard       = checks if there is a HIGH_CARD            
 * deleteLine            = deletes a line in the matrix
 * printContent          = prints an card array
 * getInMatrix           = gets cards in the auxiliary data structure             
 * 
 * If its applied it returns an one dimensional array which contains the combination.
 * Else the method returns null.
 */

import cards.*;
import cards.Card.Rank;
import cards.Card.Suit;
import static cards.Card.*;
import static cards.Card.Constant.*;

public class Analyze {
	private final int MAX = 5;
    private Card[] cards;
    private int indicator;
    public Analyze(Card[] cards){
        this.cards = cards;
    }
    
    public Card[] getCards(){
        return cards;
    }
    
    public void setCards(Card [] cards){
        this.cards = cards;
    }
    public int getN(){
    	return indicator;
    }
        
   
      
  //--------------------CHECK_BEST_HAND---------------------------------
    public Card[] checkBestHand() {
        Card[] hand = new Card[5];
       
  //--------------------CHECK_STRAIGHT_FLUSH----------------------------
        if(lookForSFlush() != null){
            hand = lookForSFlush();
            printContent(hand); System.out.printf("|STRAIGHT_FLUSH");
            indicator = 8;
            return hand;
            
  //--------------------CHECK_QUADS-------------------------------------     
        }else if(lookForQuads()!= null){
            hand = lookForQuads();
            printContent(hand); System.out.printf("|QUADS");
            indicator = 7;
            return hand;
  //--------------------CHECK_FULL_HOUSE--------------------------------
        }else if(lookForFullHouse()!= null){
            hand = lookForFullHouse();
            printContent(hand); System.out.printf("|FULL_HOUSE");
            indicator = 6;
            return hand;
  //--------------------CHECK_FLUSH-------------------------------------
        }else if(lookForFlush() != null){
            hand = lookForFlush();
            printContent(hand); System.out.printf("|FLUSH");
            indicator = 5;
            return hand;
  //--------------------CHECK_STRAIGHT----------------------------------
        }else if(lookForStraight() != null){
            hand = lookForStraight();
            printContent(hand); System.out.printf("|STRAIGHT");
            indicator = 4;
            return hand;
  //--------------------CHECK_TRIPS-------------------------------------
        }else if(lookForTrips() != null){
            hand = lookForTrips();
            printContent(hand); System.out.printf("|TRIPS");
            indicator = 3;
            return hand;
  //--------------------CHECK_TWO_PAIRS---------------------------------
        }else if(lookForTwoPairs() != null){
            hand = lookForTwoPairs();
            printContent(hand); System.out.printf("|TWO_PAIRS");
            indicator = 2;
            return hand;
  //--------------------CHECK_ONE_PAIR----------------------------------
        }else if(lookForOnePair() != null){
            hand = lookForOnePair();
            printContent(hand); System.out.printf("|ONE_PAIR");
            indicator = 1;
            return hand;
  //--------------------HIGH_CARD---------------------------------------
        }else{
            hand = lookForHighCard();
            printContent(hand); System.out.printf("|HIGH_CARD");
            indicator = 0;
            return hand;
        }
    }
        
  //--------------------STRAIGHT_FLUSH----------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *But before the filledMatrix comes in use, it will be checked if a flush is present with the cards, 
     *which are taken from the card deck.
     *If there is a flush the sFlush array get the flush cards and the suit now needs to be known.
     *At the same time it is now clear in which column of the filledMatrix the flush is.
     *So now the filledMatrix comes in use!
     *The evaluate array gets all cards from the "flush" column of filledMatrix.
     *
     *This is necessary because of the function like the lookForStraight method works:
     *Example: [CA] [C2] [C3] [C4] [C5] [S3] [CT]
     *If the lookForStraight method checks at first for a straight, it returns this cards in this order: [C5] [C4] [S3] [C2] [CA]
     *Indeed it would return a straight, but not a straight_flush.
     *Because all five cards are not in the same suit and if the lookForFlush method checks after for a flush, it returns null.
     *Otherwise the lookForFlush method could check first if there is a flush
     *but it would return this cards in this order: [CT] [C5] [C4] [C3] [C2]
     *And if the lookForStraight method checks this cards, it returns null.
     *
     *Anyway the evaluate array contains now all cards from the "flush" column of the filledMatrix, where the flush was present.
     *So the lookForStraight method just need to check if there is a straight.
     *If there is one, it returns the sFlush array.
     *Else it returns null.
     */
    public Card[] lookForSFlush(){
        Card []   cardsOrigin  = getCards();
        Card []   sFlush       = new Card [5];
        Card []   evaluate     = new Card [7];
        Card [][] filledMatrix = new Card [4][13];
        
        int i    = 0;
        int line = ACE.ordinal();
        boolean s_Flush = false;
        
        filledMatrix = getInMatrix();
        
        if(lookForFlush() != null){
            sFlush = lookForFlush();
            int column = sFlush[0].getSuit().ordinal();
                while(line >= TWO.ordinal()){
                    if(filledMatrix[column][line] != null){
                        evaluate[i] = filledMatrix[column][line];
                        i++;
                    }
                    line--;
                }//while: get all cards from the "flush" in the evaluate array
            setCards(evaluate);
            if(lookForStraight() != null){
                sFlush  = lookForStraight();
                s_Flush = true;
            }//if: checks whether the flush is a straight too
        }//if: checks whether a flush is present
        
        if(s_Flush){return sFlush;}
        else       {setCards(cardsOrigin); return null;}   
    }
     
  //--------------------CHECK_QUADS-------------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *After that the first for and while loop are looking in every column for QUADS in this way:
     *
     *ACE[ ] [ ] [ ] [ ] <== first
     *...[ ] [ ] [ ] [ ] <== ...
     *TWO[ ] [ ] [ ] [ ] <== last
     *    C   D   H   S
     *    
     *If a QUAD is present in a line the kinds array will get it and the line of the filledMatrix array will be deleted.
     *After that the next for and while loop are looking for the highest card that is left and the quad array get it.
     *Finally the method returns the quad array.
     *Else the method returns null.
     */
    public Card [] lookForQuads(){
        Card []   quad         = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        int i      = 0;  
        int line   = 0; 
        int column = 0;
        int lLine  = 0;
        final int quadsIn = 4;
        
        filledMatrix = getInMatrix();
        
        for (line = ACE.ordinal(); line >= 0; line--){
            if (i < quadsIn) {column = SPADES.ordinal(); i = 0;}
            while (column >= CLUB.ordinal()){
                if (filledMatrix[column][line] != null){
                    quad[i] = filledMatrix[column][line];
                    lLine = line;                           
                    i++;
                }
                column--;
            }//while: goes through the columns
        }//for: changes the line
                    
        if (i == quadsIn) {
            deleteLine(filledMatrix, lLine); //deletes QUAD, which already are in kinds
            for (line = ACE.ordinal(); line >= 0; line--){
                if (i < MAX) {column = SPADES.ordinal();}
                while (column >= CLUB.ordinal() && i < MAX){
                    if (filledMatrix[column][line] != null){
                        quad[i] = filledMatrix[column][line];
                        i++;
                    }
                    column--;
                }//while: goes through the columns
            }//for: changes the line
        }// if
                   
        
        if(i == MAX){return quad;}
        else        {return null;}
    }
    
  //--------------------FULL_HOUSE--------------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *After that the first for loop starts at the ACE line and the while loop at the SPADES column of the filledMatrix.
     *The while loop checks all columns at the ACE line and if there is a card, kinds array will get it.
     *If there are less than three cards present, the for loop goes a line down and the while loop checks all columns at
     *the new line.
     *If there are three cards present, it stops. fullHouse [card] [card] [card] [null] [null]
     *                                           index ==>     0      1      2    i=3     4
     *After that, the line where the trips were found the deleteLine method deletes the three cards in the filledMatrix.
     *So the next for and while loop are working at the same way like the both above, but are stopping if there are two cards
     *in a line present. fullHouse [card] [card] [card] [card] [card]
     *                index ==>       0      1      2      3      4    i=5
     *All in all if the pair is present too (i=5) the method returns the fullHouse array.
     *Else it returns null.
     */
    public Card[] lookForFullHouse(){
        Card[]   fullHouse    = new Card [5];
        Card[][] filledMatrix = new Card [4][13];
        
        int line   = 0;     final int tripsIn = 3; 
        int column = 0;     final int pairsIn = 5;
        int i      = 0;
        int lLine  = 0;
        
        filledMatrix = getInMatrix();
        
        for (line = ACE.ordinal(); line >= 0; line--){
            if    (i < tripsIn) {column = SPADES.ordinal(); i = 0;}
            while (column >= 0 && i < tripsIn) {
                if(filledMatrix[column][line] != null){
                    fullHouse[i] = filledMatrix[column][line];
                    lLine    = line;                           
                    i++;
                }
                column--;
            }//while: goes through the columns
        }//for: decrements the line
        
        if (i == tripsIn){
            filledMatrix = deleteLine(filledMatrix, lLine);
            for (line = ACE.ordinal(); line >= TWO.ordinal(); line--){
                if (i < pairsIn){column = SPADES.ordinal(); i = tripsIn;}
                while (column >= 0 && i < pairsIn){
                    if (filledMatrix[column][line] != null){ 
                        fullHouse[i] = filledMatrix[column][line];
                        i++;
                    }
                    column--;
                }//while: goes through the columns
            }//for: decrements the line
        }//if
        
        if(i == pairsIn){return fullHouse;}
        else            {return null;}
    }
    
  //--------------------FLUSH-------------------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *Now the for and while loop are going through the filledMatrix array to find a flush.
     *The for loop starts at the SPADES column and the while at the ACE line.                     END <====================== START
     *So the while loop goes through the SPADES column from the ACE line until the TWO line: | ACE[ ]  | ACE[ ]  | ACE[ ]  | ACE[ ]
     *At the same time the flush array get the cards and if there are five cards present     | ...[ ]  | ...[ ]  | ...[ ]  | ...[ ]
     *the flush array within the first five cards from the column will be returned.          ˅ TWO[ ]  ˅ TWO[ ]  ˅ TWO[ ]  ˅ TWO[ ]
     *Otherwise the for loop changes the column to the HEART column and the while                  C         D         H         S
     *loop goes accordingly through the HEART column from the ACE line until the TWO line. 
     *This repeats until the CLUB column and if there are not still five cards present                                                                       
     *the method returns null.                                                                                        
     */                                                                                       
    public Card[] lookForFlush(){
        Card []   flush        = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        int i    = 0;
        int line = ACE.ordinal();
        
        filledMatrix = getInMatrix();
        
        for(int column = SPADES.ordinal(); column >= CLUB.ordinal(); column--){
            if(i < MAX){line = ACE.ordinal(); i = 0;}
            while(line >= TWO.ordinal() && i < MAX){
                if(filledMatrix[column][line] != null){
                    flush[i] = filledMatrix[column][line];
                    i++;
                }
                line--;
            }//while: goes through the lines
        }//for: decrements the column
        
        if(i == MAX){return flush;}
        else        {return null;}
    }
    
    //------------------STRAIGHT----------------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *After that the for and while loop are beginning to find a straight.
     *The for loop begins at the ACE line and the while loop at the SPADES column.
     *
     *So the while loop goes through all columns at the line outgoing which the for loop has and if there is one card present the straight 
     *array will get the first card from that column and the while loop goes itself a line down and does not look anymore after a card.
     *For example if there are two cards present in a line [card] [null] [null] [card] the while loop changes instantly after the SPADES card
     *to the next line - the CLUB card will be ignored.      C   <=============   S
     *Then the while loop looks to the next first card in the new line and the straight array would get it if there is one present.
     *Else if there is not a card present, the while loop stops and the for loop changes the line just one down
     *outgoing from that line that the for loop had before - the search after five successive cards begins again.
     *At the time the while loop finds five cards after five successive lines, it stops and the straight array containing the five
     *cards will returned.
     *If there was not a standard straight the method lookForStraightWheel looks for the straight where the ACE is wheel.
     *Else if the lookForStraightWheel method does not find a straight too, the lookForStraight method returns null.
     */
    public Card[] lookForStraight(){
        Card []   straight     = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        int i      = 0;
        int column = SPADES.ordinal();
       
        filledMatrix = getInMatrix();
        
        for(int line = ACE.ordinal(); line >= SIX.ordinal(); line--){ 
            if(i < MAX){i = 0; column = SPADES.ordinal();} 
            while(column >= CLUB.ordinal() && i < MAX){
                if(filledMatrix[column][line]!= null){
                    straight[i] = filledMatrix[column][line];
                    i++; line--; column = SPADES.ordinal();
                }else {column--;}
                assert false : "piep";
            }//while: goes through the columns
         }//for: decrements the line
        
        if     (i == MAX){return straight;}    
        else if(lookForStraightWheel() != null){
            straight = lookForStraightWheel();
            return straight;
        }else{return null;}
    }
    
    //------------------STRAIGHT_WHEEL----------------------------------
    /*This method will just called, if there is no standard straight found in the lookForStraight method.
     *
     *So there was not a standard straight the lookForStraightWheel method will called to look after a STRAIGHT_WHEEL.
     *At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *The STRAIGHT_WHEEL looks like this for example: [C5] [S4] [H3] [D2] [SA]
     *Accordingly this method searches after four cards outgoing from the FIVE line until the TWO line.
     *If there are four cards present the last fifth card will be searched in the ACE line and if this one present too,
     *the straight array containing the STRAIGHT_WHEEL will be returned.
     *Else the method returns null.
     *
     *At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *After that the for and while loop is beginning to find the first four cards.
     *The while loop starts at the SPADES column and the FIVE line
     *
     *So the while loop goes through all columns at the line FIVE and if there is one card present the straight 
     *array will get the first card from that column and the while loop goes itself a line down and does not look anymore after a card in that line.
     *For example if there are two cards present in a line [card] [null] [null] [card] the while loop changes instantly after the SPADES card
     *to the next line - the CLUB card will be ignored.      C   <=============   S
     *Then the while loop looks to the next first card in the new line and the straight array would get it if there is one present.
     *This repeats until the TWO line if all lines before contained at least one card.
     *At the time the while loop finds four cards after the four successive lines, it stops and the straight array containing the four
     *cards will go in the second while loop.
     *The second while loop just checks all columns at the ACE line and if there is one card present the method lookForStraightWheel
     *returns the straight array.
     *Else it returns null.
     */
    public Card[] lookForStraightWheel(){
        Card []   straight     = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        int i      = 0;
        int column = SPADES.ordinal(); 
        int line   = FIVE.ordinal();
        final int firstFour = 4;
        
        filledMatrix = getInMatrix();
        
        while(column >= CLUB.ordinal() && line >= TWO.ordinal() && i<4){    
            if(filledMatrix[column][line] != null){
                straight[i] = filledMatrix[column][line];
                i++; line--; column = SPADES.ordinal();
            }else{column--;}    
        }//while: checks whether there are the first four cards present

        if(i == firstFour){
            final int rank = ACE.ordinal();
            column         = SPADES.ordinal();
            while(column >= CLUB.ordinal() && i < MAX){
                if(filledMatrix[column][rank] != null){
                    straight[i] = filledMatrix[column][rank];
                    i++;
                }
                column--;
            }//while: checks whether the ACE wheel is present
        }//if
        
        if(i == MAX){return straight;}
        else        {return null;}
    }
    
  //--------------------CHECK_TRIPS-------------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     **After that the first for and while loop are looking in every column for TRIPS in this way:
     *
     *ACE[ ] [ ] [ ] [ ] <== first
     *...[ ] [ ] [ ] [ ] <== ...
     *TWO[ ] [ ] [ ] [ ] <== last
     *    C   D   H   S
     *    
     *If a TRIP is present in a line the trips array will get it and the line of the filledMatrix array will be deleted.
     *After that the next for and while loop are looking for the last two highest cards that are left and the trips array get them.
     *Finally the method returns the trips array.
     *Else the method returns null.     
     */
    public Card[] lookForTrips(){
        Card []   trips        = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        filledMatrix = getInMatrix();
        
        int line   = 0;
        int column = 0;
        int i      = 0;
        int lLine  = 0;
        final int tripsIn = 3;
        
        for (line = ACE.ordinal(); line >= TWO.ordinal(); line--){
            if (i < tripsIn) {column = SPADES.ordinal(); i = 0;}
            while (column >= CLUB.ordinal() && i < tripsIn){
                if (filledMatrix[column][line] != null){
                    trips[i] = filledMatrix[column][line];
                    lLine    = line;                           
                    i++;
                }
                column--;
            }//while: goes through the columns
        }// for: decrements the line
        
        if (i == tripsIn){                                       
            filledMatrix = deleteLine(filledMatrix, lLine);
            for (line = ACE.ordinal(); line >= TWO.ordinal(); line--){   
                if (i < MAX) {column = SPADES.ordinal();}
                while (column >= CLUB.ordinal() && i < MAX){
                    if (filledMatrix[column][line] != null){ 
                        trips[i] = filledMatrix[column][line];
                        i++;
                    }
                    column--;
                }//while: goes through the columns
            }//for: decrements the line
        }//if

        if(i == MAX){return trips;}
        else        {return null;}  
    }
    
  //--------------------CHECK_TWO_PAIRS---------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *After that the first for and while loop are looking in every column for TWO_PAIRS in this way:
     *
     *ACE[ ] [ ] [ ] [ ] <== first
     *...[ ] [ ] [ ] [ ] <== ...
     *TWO[ ] [ ] [ ] [ ] <== last
     *    C   D   H   S
     *    
     *If a pair is present in a line the twoPairs array will get it and the line of the filledMatrix array will be deleted.
     *Then the second pair will be searched and if it is present too the twoPairs array will get it and this line of the filledMatrix
     *array will be deleted also.
     *After that the next for and while loop are looking for the last highest card that is left and the twoPairs array get it.
     *Finally the method returns the twoPairs array.
     *Else the method returns null.     
     */
    public Card[] lookForTwoPairs(){
        Card []   twoPairs     = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        int line   = 0; 
        int column = 0; 
        int i      = 0; 
        int j      = 0;
        int lLine  = 0;
        final int firstPairIn = 2, secondPairIn = 4;
        
        filledMatrix = getInMatrix();
        
        for (line = ACE.ordinal(); line >= 0; line--){
            if     (j < firstPairIn)                     {column = SPADES.ordinal(); i = 0; j = 0;}
            else if(i >= firstPairIn && i < secondPairIn){column = SPADES.ordinal(); i = firstPairIn;}
            while (column >= CLUB.ordinal() && i < secondPairIn){
                if (filledMatrix[column][line] != null){
                    twoPairs[i] = filledMatrix[column][line];
                    lLine    = line;                                        
                    i++; j++;
                }
                if(j == firstPairIn) {filledMatrix = deleteLine(filledMatrix, lLine);} //deletes first pair
                if(i == secondPairIn){filledMatrix = deleteLine(filledMatrix, lLine);} //deletes second pair
                column--;
            }//while: goes through the columns
        }//for: decrements the line
        
        if (i == secondPairIn){ 
            for (line = ACE.ordinal(); line >= TWO.ordinal(); line--){
                if (i < 5) {column = SPADES.ordinal();}
                while (column >= CLUB.ordinal() && i < MAX){
                    if (filledMatrix[column][line] != null){ 
                        twoPairs[i] = filledMatrix[column][line];
                        i++;
                    }
                    column--;
                }//while: goes through the columns
            }//for: decrements the line
        }//if

        if(i==5){return twoPairs;}
        else    {return null;}
    }
    
  //--------------------CHECK_ONE_PAIR----------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *After that the first for and while loop are looking in every column for ONE_PAIR in this way:
     *
     *ACE[ ] [ ] [ ] [ ] <== first
     *...[ ] [ ] [ ] [ ] <== ...
     *TWO[ ] [ ] [ ] [ ] <== last
     *    C   D   H   S
     *    
     *If a PAIR is present in a line the onePair array will get it and the line of the filledMatrix array will be deleted.
     *After that the next for and while loop are looking for the last three highest cards that are left and the onePair array get them.
     *Finally the method returns the onePair array.
     *Else the method returns null.     
     */
    public Card[] lookForOnePair(){
        Card []   onePair      = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        int line   = 0; 
        int column = 0; 
        int i      = 0;
        int lLine  = 0;
        final int onePairIn = 2;
        
        filledMatrix = getInMatrix();
        
        for (line = ACE.ordinal(); line >= TWO.ordinal(); line--){
            if (i < onePairIn) {column = SPADES.ordinal(); i = 0;}
            while (column >= CLUB.ordinal() && i < onePairIn){
                if (filledMatrix[column][line] != null){
                    onePair[i] = filledMatrix[column][line];
                    lLine = line;
                    i++;
                }
                column--;
            }//while: goes through the columns
        }//for: decrements the line
        
        if (i == onePairIn) {
            filledMatrix = deleteLine(filledMatrix, lLine);
            for (line = ACE.ordinal(); line >= TWO.ordinal(); line--){
                if (i < MAX) {column = SPADES.ordinal();}
                while (column >= CLUB.ordinal() && i < MAX){
                    if (filledMatrix[column][line] != null){ 
                        onePair[i] = filledMatrix[column][line];
                        i++;
                    }
                    column--;
                }//while: goes through the columns
            }//for: decrements the line
        }//if

        if(i == MAX){return onePair;}
        else        {return null;}
    }
    
    //------------------HIGH_CARD---------------------------------------
    /*At first the filledMatrix array will get a copy of the auxiliary data structure with the cards on their
     *corresponding places.
     *The for and while loop are beginning at the ACE line and the SPADES column.
     *After the while loop is finished with the columns from SPADES till CLUB at the ACE line
     *it goes again through the SPADES till CLUB column at the KING line.
     *This repeats till the TWO line and if there are found five cards before the highCard array get 
     *so first highest cards and will be returned.
     *Else the method returns null.
     */
    public Card [] lookForHighCard(){
        Card []   highCard     = new Card [5];
        Card [][] filledMatrix = new Card [4][13];
        
        int i      = 0;
        int column = 0;
        
        filledMatrix = getInMatrix();
        
        for(int line = ACE.ordinal(); line >= TWO.ordinal(); line--){
            if(i < MAX){column = SPADES.ordinal();}
            while(column >= CLUB.ordinal() && i < MAX){    
                if(filledMatrix[column][line] != null){
                highCard[i] = filledMatrix[column][line];
                i++;
                }
                column--;
            }//while: goes through the columns
        }//for: decrements the line
        
        if(i == MAX){return highCard;}
        else        {return null;}
    }
    
    //------------------DELETE_LINE-------------------------------------
    public Card[][] deleteLine(Card [][] modifiedMatrix, int lastLine){
        
        
        for (int column = SPADES.ordinal(); column >= CLUB.ordinal(); column--) { // the deletion begins at lastLine
            modifiedMatrix[column][lastLine] = null;                    // the columns from SPADES until CLUB will be overwrite with null
        }
        return modifiedMatrix;
    }
    
  //--------------------PRINT_CONTENT-----------------------------------
    public void printContent(Card [] content) {
        for (int i = 0; i < content.length; i++) {
            if(content[i] != null){
            System.out.printf("%s", content[i]);
            }
        }
    }

  //--------------------GET_IN_MATRIX-----------------------------------
    /**
     * This class contains an auxiliary data structure in form of an 4x13 Array.
     * The method "getInMatrix" gets cards and place them on their corresponding places.
     * Below the auxiliary data structure is shown with the line indexes from 0 till 12 for the ranks and with the columns
     * from from 0 till 3 for the Suits:
     *
     * Ranks 
     *  |
     *  ˅
     * ACE    12 [ ] [ ] [ ] [ ]     I used this data structure, because its easy to handle.
     * KING   11 [ ] [ ] [ ] [ ]     The reason is, that you can get with "cards[x].getSuit().ordinal()" the int-Enum
     * Queen  10 [ ] [ ] [ ] [ ]     of the Suit that the card has.
     * JACK   9  [ ] [ ] [ ] [ ]     For a SPADE card it returns 3, for HEART 2, for DIAMOND 1 and for CLUB 0.
     * TEN    8  [ ] [ ] [ ] [ ]     So this fits to the column indexes.
     * NINE   7  [ ] [ ] [ ] [ ]     At the similar way, you can get the int-Enum for a rank.
     * EIGHT  6  [ ] [ ] [ ] [ ]     "cards[x].getRank().ordinal()" returns for ACE 12, for KING 11 and so on like
     * SEVEN  5  [ ] [ ] [ ] [ ]     it is shown in the visualization at the left.
     * SIX    4  [ ] [ ] [ ] [ ]     Anyway, so it is easier to search after the different combinations in Texas Hold´em.
     * FIVE   3  [ ] [ ] [ ] [ ]     Example:
     * FOUR   2  [ ] [ ] [ ] [ ]     For a flush you just need to count if there are five cards in a column.
     * THREE  1  [ ] [ ] [ ] [ ]
     * TWO    0  [ ] [ ] [ ] [ ]
     *            0   1   2   3  
     * Suits ==>  C   D   H   S  
     *            L   I   E   P
     *            U   A   A   A
     *            B   M   R   D
     *                O   T   E
     *                N       S
     *                D
     */
    
    /*This method get the Rank and the Suit of the cards one by one in an one dimensional array.
     *After Rank and Suit are known, the 4x13 matrix array get the card on its corresponding place.
     */
    public Card[][] getInMatrix(){
        Card[][] matrix = new Card [4][13];
        
        int rank;
              
        for (int i = 0; i < cards.length; i++) {                                         //for_loop to go through the one dimensional array
            if(cards[i] != null){                                                        //if a card exists on the index i..
                 rank = cards[i].getRank().ordinal();                                    //..first the rank will be get..
                switch (cards[i].getSuit()) {                                            //..then the suit..
                    case CLUB    : matrix [CLUB.ordinal()]    [rank] = cards[i]; break;  //..finally the card will be set on its corresponding place. 
                    case DIAMOND : matrix [DIAMOND.ordinal()] [rank] = cards[i]; break;
                    case HEART   : matrix [HEART.ordinal()]   [rank] = cards[i]; break;
                    case SPADES  : matrix [SPADES.ordinal()]  [rank] = cards[i]; break;
                   }
            }//if
        }
        return matrix;
    }//for
  //--------------------------------------------------------------------
    
}//Analyze
