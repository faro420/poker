package poker.test;


@ClassPreamble (
    vcs             = "bitbucket.org/schaefers/poker.git",
    author          = "Michael Sch�fers",
    contact         = "schafers@informatik.haw-hamburg.de",
    organization    = "Dept.Informatik; HAW Hamburg",
    date            = "2012/11/19",
    version         = "3.03",
    note            = "release for SS14 ;  1st release WS08/09",
    lastModified    = "2014/05/29",
    lastModifiedBy  = "Michael Sch�fers",
    reviewers       = ( "none" )
)
public enum HandRanking {
    HIGH_CARD,
    ONE_PAIR,
    TWO_PAIR,
    TRIPS,
    STRAIGHT,
    FLUSH,      
    FULL_HOUSE,
    QUADS,
    STRAIGHT_FLUSH;
    
    //___under_development______________________________________________________
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    static final private CID cid = new CID();
    //
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    static final private class CID {                                            // Class IDentification
        @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
        private CID(){
            final Registration registration = Registration.getInstance();
            registration.registrate( getClass() );
        }//constructor()
    }//class
    
}//enum
