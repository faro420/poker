package poker.test;


import java.util.*;


@ClassPreamble (
    vcs             = "bitbucket.org/schaefers/poker.git",
    author          = "Michael Sch�fers",
    contact         = "schafers@informatik.haw-hamburg.de",
    organization    = "Dept.Informatik; HAW Hamburg",
    date            = "2014/05/28",
    version         = "3.03",
    note            = "release for SS14 ;  1st release SS14",
    lastModified    = "2014/05/30",
    lastModifiedBy  = "Michael Sch�fers",
    reviewers       = ( "none" )
)
final public class Registration {
    
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    class Data {
        
        @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
        private Data( final Class<?> clss,  final Package pckg ){
            this.clss = clss;
            this.pckg = pckg;
        }//constructor()
        
        
        
        @ChunkPreamble ( lastModified="2014/05/29", lastModifiedBy="Michael Sch�fers" )
        Class<?> getClss(){ return clss; }
        
        @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
        String getPackagePath(){ return pckg.getName(); }
        
        @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
        @Override
        public String toString(){
            return String.format(
                "%s -> %s",
                clss.getSimpleName(),
                pckg.getName()
            );
        }//method()
        
        
        
        @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
        final Class<?> clss;
        
        @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
        final Package  pckg;
        
    }//class
    
    
    
    
    
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    private Registration(){
        list = new ArrayList<Data>();
    }//constructor
    
    
    
    
    
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    static public Registration getInstance(){ return instance; }
    
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    static final private Registration instance = new Registration();
    
    
    
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    public final void registrate( final Class<?> clss ){
        final Class<?> enclosingClass = clss.getEnclosingClass();
        list.add( new Data( enclosingClass, enclosingClass.getPackage() ) );
    }//method()
    
    
    
    
    
    @ChunkPreamble ( lastModified="2014/05/30", lastModifiedBy="Michael Sch�fers" )
    private final List<Data> list;      public List<Data> getList(){ return list; }
    
}//class
