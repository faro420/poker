package poker;

import cards.*;
import cards.Card.Rank;
import cards.Card.Suit;
import static cards.Card.*;
import static cards.Card.Constant.*;

public class ManuallyForB1 {

    public void manuallyB1(){
    
    //------------------------------------------------------
    /*
     *  A card array for every will be initialized 
     *  with two cards as hole cards.
     */
        
    Card [] p1 = {CJ, SJ};
    Card [] p2 = {HT, DT};
    Card [] p3 = {S2, SA};
    Card [] p4 = {SK, S4};
    Card [] p5 = {CQ, HQ};
    
    //A card array with five cards will be initialized as community cards.
    Card [] comCards = {D5, H5, SQ, S5, C5};
    
  //---------------------------------------------------------
    // Five Players are created here.
    
    Player player1 = new Player("Farshi");
    Player player2 = new Player("Mehmet");
    Player player3 = new Player("Susana");
    Player player4 = new Player("Johnny");
    Player player5 = new Player("Badass");
    
    //---------------------------------------------------------
    //Every players hand will be set with his cards.
    player1.setHand(p1);
    player2.setHand(p2);
    player3.setHand(p3);
    player4.setHand(p4);
    player5.setHand(p5);
    
    //---------------------------------------------------------
    
    Evaluator player = new Evaluator(player1, player2, player3, player4, player5);
    player.setComCards(comCards);
    player.showHoleandComCards();
    player.lookForComb();
    player.showDown();
    player.finalRankings();
    
    
    }
    
}
