package poker;


import java.text.*;
import java.util.*;
import poker.test.*;


@ClassPreamble (
    vcs             = "bitbucket.org/schaefers/poker.git",
    author          = "Michael Sch�fers",
    contact         = "schafers@informatik.haw-hamburg.de",
    organization    = "Dept.Informatik; HAW Hamburg",
    date            = "2012/11/19",
    version         = "3.03",
    note            = "release for SS14 ;  1st release WS08/09",
    lastModified    = "2014/05/30",
    lastModifiedBy  = "Michael Sch�fers",
    reviewers       = ( "none" )
)
public class TestFrame {
    
    @ChunkPreamble ( lastModified="2014/05/30", lastModifiedBy="Michael Sch�fers" )
    public static void main( final String[] unused ){
        
        System.out.printf( "\n\n\n" );
        System.out.printf( "###############################################################################\n" );
        System.out.printf( "###############################################################################\n" );
        System.out.printf( "###############################################################################\n" );
        System.out.printf( "### Informationen zum Environment:\n" );
        System.out.printf( "### ==============================\n" );
        System.out.printf( "###\n" );
        System.out.printf( "### Java:     %s bzw. %s\n",       System.getProperty( "java.specification.version" ), System.getProperty( "java.version" ) );
        System.out.printf( "### O.-P.:    %s\n",               new Object().getClass().getPackage() );
        System.out.printf( "### %s is running\n",              cid.getClass().getEnclosingClass().getCanonicalName() );
        System.out.printf( "###\n" );
        System.out.printf( "###\n" );
        System.out.printf( "### STARTING @ %s\n", new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss.SSS", new Locale("de","DE") ).format( new Date().getTime() ) );
        System.out.printf( "\n" );
        System.out.flush();
        
        final GameAnalyzer ga = new DummyForYourSolution();
        final TestExecutor te = new FieldSimulator( ga );
        te.execute();
        
        System.out.printf( "###############################################################################\n" );
        System.out.printf( "###############################################################################\n" );
        System.out.printf( "### THE END @ %s", new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss.SSS", new Locale("de","DE") ).format( new Date().getTime() ) );
        
    }//method()
    
    
    
    //___under_development______________________________________________________
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    static final private CID cid = new CID();
    //
    @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
    static final private class CID {                                            // Class IDentification
        @ChunkPreamble ( lastModified="2014/05/28", lastModifiedBy="Michael Sch�fers" )
        private CID(){
            final Registration registration = Registration.getInstance();
            registration.registrate( getClass() );
        }//constructor()
    }//class
    
}//class
