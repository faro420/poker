package poker;

import cards.*;
import cards.Card.Rank;
import cards.Card.Suit;
import static cards.Card.*;
import static cards.Card.Constant.*;

public class DealForB1 {

    Deck d = new Deck();
    
    public void dealB1(){
      //---------------------------------------------------------
        /*
         * Two Cards will be pulled for every player, 
         * to set every players hand with his cards.
         */
        
        Card [] p1 = new Card [2];
        Card [] p2 = new Card [2];
        Card [] p3 = new Card [2];
        Card [] p4 = new Card [2];
        Card [] p5 = new Card [2];
        
        p1 = pullCards(p1);
        p2 = pullCards(p2);
        p3 = pullCards(p3);
        p4 = pullCards(p4);
        p5 = pullCards(p5);
        
        //---------------------------------------------------------
        // Five Players are created here.
        
        Player player1 = new Player("Farshi");
        Player player2 = new Player("Mehmet");
        Player player3 = new Player("Susana");
        Player player4 = new Player("Johnny");
        Player player5 = new Player("Badass");
        
        //---------------------------------------------------------
        //Every players hand will be set with his cards.
        player1.setHand(p1);
        player2.setHand(p2);
        player3.setHand(p3);
        player4.setHand(p4);
        player5.setHand(p5);
        
        //---------------------------------------------------------
        Card [] comCards = new Card [5];
        
        comCards = pullCards(comCards);
        
        
        
        Evaluator player = new Evaluator(player1, player2, player3, player4, player5);
        player.setComCards(comCards);
        player.showHoleandComCards();
        player.lookForComb();
        player.showDown();
        player.finalRankings();
    }
    
    public Card[] pullCards( Card [] holeCards){
        for(int i = 0; i < holeCards.length; i++){
            holeCards[i] = d.deal();
        }
        return holeCards;
    }
    
}
