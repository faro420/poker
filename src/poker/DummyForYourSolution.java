// release for SS14 by Michael Sch�fers;  1st release WS07/08 by Michael Sch�fers
// lastModified 2014/05/08 by Michael Sch�fers
package poker;


// just import "anything" in case it might be needed
//
import static cards.Card.*;
import static cards.Card.Constant.*;
import static poker.test.HandRanking.*;
//
import cards.*;
import cards.Card.*;
import poker.test.*;


public class DummyForYourSolution implements GameAnalyzer {
    
    @Override
    public ResultOfGame compute( final TestCaseActual tca ){
        
        // Zugriff auf die Karten
        // ======================
        
        Card [] p1 = tca.getPlayerHoleCards( 0 );         // player #1
        Card [] p2 = tca.getPlayerHoleCards( 1 );         // player #2
        Card [] p3 = tca.getPlayerHoleCards( 2 );         // player #3
        Card [] p4 = tca.getPlayerHoleCards( 3 );         // player #4
        Card [] p5 = tca.getPlayerHoleCards( 4 );         // player #5
        
        Card [] comCards = tca.getCommunityCards();             // 
        
        
        
        
        
        // Ihre Berechnung
        // ===============
      
        Player player1 = new Player ("p1");
        Player player2 = new Player ("p2");
        Player player3 = new Player ("p3");
        Player player4 = new Player ("p4");
        Player player5 = new Player ("p5");
        
        player1.setHand(p1);
        player2.setHand(p2);
        player3.setHand(p3);
        player4.setHand(p4);
        player5.setHand(p5);
        
        
        Evaluator game = new Evaluator(player1, player2, player3, player4, player5);
        game.setComCards(comCards);
        game.lookForComb();
        game.showDown();game.finalRankings();
        
        HandRanking p1r = HandRanking.values()[player1.getRank()];
        HandRanking p2r = HandRanking.values()[player2.getRank()];
        HandRanking p3r = HandRanking.values()[player3.getRank()];
        HandRanking p4r = HandRanking.values()[player4.getRank()];
        HandRanking p5r = HandRanking.values()[player5.getRank()];
        // Abliefern des Ergebnisses
        // =========================
        
        ResultOfGame rog = null;
        
        // Alternative 1: "Direkt"
        rog = new ResultOfGame(
            new ResultOfPlayer[]{
                //
                //                  /------------------------------------------------------ position of player:
                //                  |                                                       1 is the best, .., 5 is the worst
                //                  |
                //                  |   /-------------------------------------------------- handranking of player - valid values are:
                //                  |   |                                                   STRAIGHT_FLUSH, QUADS, FULL_HOUSE, FLUSH, STRAIGHT, 
                //                  |   |                                                   TRIPS, TWO_PAIR, ONE_PAIR and HIGH_CARD.
                //                  |   |                                                   These values are defined in the enum poker.test.HandRanking
                //                  |   |
                //                  |   |           /-------------------------------------- cards of player that are played (to be computed by your solution)
                //                  |   |           |
                //                  V   V           V
                new ResultOfPlayer( player1.getPlace(),  p1r,  player1.getHand() ),  // player #1 ;  just an example - it's not that easy ;-)
                new ResultOfPlayer( player2.getPlace(),  p2r,  player2.getHand() ),  // player #2 ;  just an example - it's not that easy ;-)
                new ResultOfPlayer( player3.getPlace(),  p3r,  player3.getHand() ),  // player #3 ;  just an example - it's not that easy ;-)
                new ResultOfPlayer( player4.getPlace(),  p4r,  player4.getHand() ),  // player #4 ;  just an example - it's not that easy ;-)
                new ResultOfPlayer( player5.getPlace(),  p5r,  player5.getHand() )   // player #5 ;  just an example - it's not that easy ;-)
            }//array-new
        );//new
        
        
        
        // Viele weitere Varianten sind moeglich
        
        // Moegliche Werte fuer "HandRanking" sind die 9 Werte:  HIGH_CARD, ONE_PAIR, TWO_PAIR, TRIPS, STRAIGHT, FLUSH, FULL_HOUSE, QUADS, STRAIGHT_FLUSH
        // Bemerkung: Diese Werte sind in poker.test.HandRanking als enum definiert. Sie duerfen dieser Werte weder veraendern noch ergaenzen.
        
        return rog;
    }//method()
    
}//class
