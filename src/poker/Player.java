package poker;

import cards.Card;

public class Player {
    private int rank;
    private int place;
    private Card[] hand;
    private String name;

    public Player(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public Card[] getHand() {
        return hand;
    }

    public void setHand(Card[] hand) {
        this.hand = hand;
    }

    public String getName() {
        return name;
    }

    public boolean equals(Player other) {
        return other.getClass() == this.getClass() && this.rank == other.rank;
    }
}
