package poker;

import cards.*;
import cards.Card.Rank;
import cards.Card.Suit;
import static cards.Card.*;
import static cards.Card.Constant.*;
import tools.Analyze;

public class Evaluator {

    private Card[] comCards = new Card[5];
    private Player[] players;

    private boolean unsorted;
    private boolean pulled   = false;
    private boolean swapIt   = false;
    
    public Evaluator(Player... players) {
        assert players.length == 5;
        this.players = players;
    }
    
    public void setComCards(Card [] comCards){
        this.comCards = comCards;
    }

    // ------------LOOK_FOR_COMB--------------------
    /*
     * Players get one by one community cards to
     * see later for combination.
     */
    public void lookForComb() {
        for (int i = 0; i < 5; i++)
            combinedHand(players[i]);
    }

    // -----------COMBINED_HAND-----------------------
    /*
     * The player gets here his combined hand.Two hand cards plus community
     * cards.
     */
    public void combinedHand(Player player) {
        Card[] holeCards = player.getHand();
        Card[] holePlusCom = new Card [7];
        
        for (int i = 0; i < 2; i++){
            holePlusCom[i] = holeCards[i];
        }
        
        for (int i = 2; i < 7; i++) {
            holePlusCom[i] = comCards[i - 2];
        }
        player.setHand(holePlusCom);
    }
    
    
    //----------------------------------------
    public void showHoleandComCards(){
        
        System.out.printf("THE PLAYERS PULLED\n");
        System.out.printf("==================\n");
        for (int i = 0; i < players.length; i++){
            System.out.printf("%s", players[i].getName());
            Analyze o = new Analyze (players[i].getHand());
            o.printContent(players[i].getHand());
            System.out.printf("\n");
        }
        
        System.out.printf("\nCOMMUNITY CARDS\n");
        System.out.printf("===============\n");
        Analyze o = new Analyze (comCards);
        o.printContent(comCards);
        System.out.printf("\n");
    }
    

    // ---------SHOWDOWN-----------------------
    /*
     * SHOWDOWN method just calls playBestHandmethod, to get best hand into the
     * corresponding player array.
     */
    public void showDown() {
        System.out.printf("\nSHOWDOWN\n");
        System.out.printf("========\n");
        for (int i = 0; i < 5; i++)
            playBestHand(players[i]);

    }

    // --------------PLAY_BEST_HAND-------------------
    /*
     * Gets an card array, checks which best combinations possible. Then it
     * returns the array with the best possible combination.
     */
    public void playBestHand(Player player) {
        Analyze c = new Analyze(player.getHand());
        System.out.print(player.getName());
        player.setHand(c.checkBestHand());
        player.setRank(c.getN());
        System.out.printf("\n");
        pulled = true;
    }

    // --------------FINAL_RANKINGS-------------------
    public void finalRankings() {
        if (pulled) {
            int[] matrixRed = new int[5];
            int pIndexA, pIndexB;
            for(int i = 0; i < players.length; i++){
                matrixRed[i] = i;
            }
            
            unsorted = true;
            while(unsorted){
                unsorted = false;
                for (int i = 0; i < 4; i++) {
                    pIndexA = matrixRed[i];
                    pIndexB = matrixRed[i + 1];
                    
                    if(players[pIndexA].getRank() < players[pIndexB].getRank()){
                        swapPositions(matrixRed, i);
                    }
                }
            }
            
            unsorted = true;
            while (unsorted) {
                unsorted = false;
                for (int i = 0; i < 4; i++) {
                    pIndexA = matrixRed[i]; // getting players index at position i
                    pIndexB = matrixRed[i + 1]; // getting players index at position i + 1

                    if (players[pIndexA].equals(players[pIndexB])) {
                        matrixRed = evaluateSameRanks(matrixRed, players[pIndexA].getRank(), i, pIndexA, pIndexB);
                    }
                }
            }// while
            int place = 2;
            int pIndexFirst = matrixRed[0];
            players[pIndexFirst].setPlace(1); // first player gets the place 1

            for (int i = 0; i < 4; i++) {
                pIndexA = matrixRed[i]; // getting players index at position i
                pIndexB = matrixRed[i + 1]; // getting players index at position
                                            // i + 1

                if (players[pIndexA].equals(players[pIndexB]) && allEqual(pIndexA, pIndexB)) {
                    players[pIndexB].setPlace(players[pIndexA].getPlace());
                } else {
                    players[pIndexB].setPlace(place);
                }
                place++;
            }

            //Just an output of the ranking
            System.out.printf("\nRANKING\n");
            System.out.printf("=======");
            int pIndex = 0, oneTime;
            for (place = 1; place < 6; place++) {
                oneTime = 0;
                for (int i = 0; i < matrixRed.length; i++) {
                    pIndex = matrixRed[i];
                    if (players[pIndex].getPlace() == place) {
                        if (oneTime == 0) {
                            System.out.printf("\n%d. PLACE ", place);
                            oneTime = 1;
                        }
                        System.out.printf(" %s", players[pIndex].getName());
                    }
                }
            }
        }//if
    }

    // -------------EVALUATE_SAME_RANKS------------------------------------
    // Evaluation in case multiple players have got the same preliminary
    // evaluation rank
    public int[] evaluateSameRanks(int[] matrixRed, int cases, int position, int pIndexA, int pIndexB) {
        Card[] playerAHand = players[pIndexA].getHand();
        Card[] playerBHand = players[pIndexB].getHand();
        switch (cases) {
        case 0:
            checkFromTill(playerAHand, playerBHand, 0, playerAHand.length);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 1:
            checkFromTill(playerAHand, playerBHand, 1, playerAHand.length);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 2:
            checkFromTill(playerAHand, playerBHand, 1, playerAHand.length);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 3:
            checkFromTill(playerAHand, playerBHand, 2, playerAHand.length);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 4:
            checkFromTill(playerAHand, playerBHand, 0, 1);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 5:
            checkFromTill(playerAHand, playerBHand, 0, playerAHand.length);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 6:
            checkFromTill(playerAHand, playerBHand, 2, 4);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 7:
            checkFromTill(playerAHand, playerBHand, 3, playerAHand.length);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        case 8:
            checkFromTill(playerAHand, playerBHand, 0, 1);
            if (swapIt) {
                matrixRed = swapPositions(matrixRed, position);
            }
            break;
        }
        return matrixRed;
    }

    // -------------------SWAP_POSITIONS------------------------
    // Swaps the positions of the players
    public int[] swapPositions(int[] matrixRed, int position) {
        int tmp;

        tmp = matrixRed[position];
        matrixRed[position] = matrixRed[position + 1];
        matrixRed[position + 1] = tmp;
        unsorted = true;

        return matrixRed;
    }

    // --------------CHECK_FROM_TILL----------------------------------------
    public void checkFromTill(Card[] playerAHand, Card[] playerBHand, int start, int end) {
        swapIt = false;

        for (int i = start; i < end; i++) {
            if (playerAHand[i].getRank().value() < playerBHand[i].getRank().value()) {
                i = end;
                swapIt = true;
            } else if (playerAHand[i].getRank().value() > playerBHand[i].getRank().value()) {
                i = end;
                swapIt = false;
            }
        }
    }

    // -------------CHECK_ALL_EQUAL------------------------------------------
    public boolean allEqual(int pIndexA, int pIndexB) {
        Card[] playerAHand = players[pIndexA].getHand();
        Card[] playerBHand = players[pIndexB].getHand();
        int equalCounter = 0;

        for (int i = 0; i < playerAHand.length; i++) {
            if (playerAHand[i].getRank().value() == playerBHand[i].getRank().value()) {
                equalCounter++;
            } else {
                i = playerAHand.length;
            }
        }
        return equalCounter == 5;
    }
    // ----------------------------------------------------------------------
}// Player
