package poker;

import cards.*;
import cards.Card.Rank;
import cards.Card.Suit;
import static cards.Card.*;
import static cards.Card.Constant.*;
import tools.Analyze;

public class PokerA7 {
    public static void main(String[] args) {
        // Deck d = new Deck();
        //
        // Card[] cards = new Card[7];
        //
        // for(int i=0; i < cards.length; i++){
        // cards[i] = d.deal();
        // System.out.printf("%s", cards[i]);
        // }
        // System.out.printf("\n\n");
        // =======================================================
        // test cases for flush
        // Card [] cards = {CA, SK, CQ, SJ, ST, S7, S8};
        // Card [] cards = {CA, C2, CQ, CJ, CT, S7, S8};
        // Card [] cards = {DA, DK, DQ, SJ, DT, D7, D8};
        // Card [] cards = {HA, HK, SA, HJ, DT, H7, H8};
        // =======================================================

        // =====================================================================
        // test cases for straight

        // Card [] cards = {CA, SK, CQ, HJ, ST, D2, C2}; //straight 1
        // Card [] cards = {CK, SJ, CQ, H9, ST, D8, C8}; //straight 2
        // Card [] cards = {C3, SJ, CQ, H9, ST, D7, C8}; //straight 3
        // Card [] cards = {C2, SJ, C3, H9, ST, D7, C8}; //straight 4
        // Card [] cards = {C2, S3, C6, H9, ST, D7, C8}; //straight 5
        // Card [] cards = {C2, S6, CQ, H9, S5, D7, C8}; //straight 6
        // Card [] cards = {CK, SJ, C4, H5, S6, D7, C8}; //straight 7
        // Card [] cards = {CK, SA, C3, H4, S5, D7, C6}; //straight 8
        // Card [] cards = {C6, S8, C5, H9, S4, D2, C3}; //straight 9
//         Card [] cards = {SA, S5, C5, C4, CA, C2, C3}; //straight 10
        // =====================================================================

        // =====================================================================
        // Card [] cards = {C2, D2, H2, S2, C8, HA, C6};
        // Card [] cards = {C3, D3, H5, S5, C7, H7, C2}; // 3*two pairs -> two
        // pairs
//         Card [] cards = {C2, D3, H2, S3, CA, H3, C2}; // 2*trips -> full
        // house
        // Card [] cards = {C3, C8, H3, S2, C2, C7, CT};
        // =====================================================================
        // test cases for trips
//         Card [] cards = {H4, C8, H3, S2, C2, C7, D2};
        //
//         Analyze c = new Analyze (cards);
//         c.checkBestHand();
        //
    }// main
}
