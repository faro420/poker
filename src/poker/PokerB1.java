package poker;

import cards.*;
import cards.Card.Rank;
import cards.Card.Suit;
import static cards.Card.*;
import static cards.Card.Constant.*;
import tools.Analyze;

/*This Poker task works with an auxiliary data structure in form of an 4x13 Array. (more in Matrix Class)
 * 
 */

public class PokerB1 {

    
    
    public static void main(String[] args) {

        // Player player1 = new Player("Farshi");
        // Player player2 = new Player("Mehmet");
        // Player player3 = new Player("Susana");
        // Player player4 = new Player("Johnny");
        // Player player5 = new Player("Badass");
        //
        // GameAnalyzer player = new GameAnalyzer(player1, player2, player3,
        // player4, player5);
        // player.pullCards();
        // player.showDown();
        // player.finalRankings();

//-------------CHEAT_MODULE------------------------------
//        PokerB1 test = new PokerB1();
//        System.out.println(test.cheatModule());
//-------------------------------------------------------

        //------------DEALING_AND_MANUALLY-----------------------------------
        
        // It is better to shut off one of the methods by putting it in comments
        // Both methods at same time are at least for me too much.

//        DealForB1 d = new DealForB1(); // In the Class DealForB1 community cards and hole cards
//        d.dealB1();                    // will be get automatically.

        ManuallyForB1 m = new ManuallyForB1(); // In the Class ManuallyForB1 community cards and hole cards
        m.manuallyB1();                        // can be set manually

      //------------DEALING_AND_MANUALLY_END-----------------------------------

    }

//    public boolean cheatModule() {
//        Player player1 = new Player("Farshi");
//        Player player2 = new Player("Mehmet");
//        Player player3 = new Player("Susana");
//        Player player4 = new Player("Johnny");
//        Player player5 = new Player("Badass");
//
//        GameAnalyzer game = new GameAnalyzer(player1, player2, player3, player4, player5);
//        game.pullCards();
//        game.showDown();
//        Card[] hand1 = { S5, D5, C5, C4, H4 };// full house rank 7
//        Card[] hand2 = { SA, CA, HA, H4, C4 };// full house rank 7
//        Card[] hand3 = { SA, HA, C5, C4, D2 };// one pair rank 2
//        Card[] hand4 = { S2, H2, CA, C5, C4 };// one pair rank 2
//        Card[] hand5 = { SA, SK, HQ, CJ, DT };// straight rank 5
//
//        player1.setHand(hand1);
//        player1.setRank(7);
//        player2.setHand(hand2);
//        player2.setRank(7);
//        player3.setHand(hand3);
//        player3.setRank(2);
//        player4.setHand(hand4);
//        player4.setRank(2);
//        player5.setHand(hand5);
//        player5.setRank(5);
//        game.finalRankings();
//        System.out.println("\n" + player1.getPlace());
//        System.out.println("\n" + player2.getPlace());
//        System.out.println("\n" + player3.getPlace());
//        System.out.println("\n" + player4.getPlace());
//        System.out.println("\n" + player5.getPlace());
//
//        return player1.getPlace() == 2 && player2.getPlace() == 1 && player3.getPlace() == 4 && player4.getPlace() == 5 && player5.getPlace() == 3;
//    }
 
    
    
    
}

